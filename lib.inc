section .text ; by Tsenekov D.

%define READ_SYSCALL_NUMBER 0
%define WRITE_SYSCALL_NUMBER 1
%define EXIT_SYSCALL_NUMBER 60
%define STD_IN 0
%define STD_OUT 1

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, EXIT_SYSCALL_NUMBER
	syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.counter:
		inc rax
		cmp byte[rdi+rax-1], 0
		jne .counter
	dec rax
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi ; pointer
	mov  rdx, rax ; length
	mov  rax, WRITE_SYSCALL_NUMBER
	mov  rdi, STD_OUT
	syscall
	ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, '\n'

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov  rsi, rsp ; pointer
	mov  rdx, 1 ; length
	mov  rax, WRITE_SYSCALL_NUMBER
	mov  rdi, STD_OUT
	syscall
	pop rdi
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jge print_uint ; num >= 0
	push rdi
	mov rdi, '-'
	call print_char ; print '-'
	pop rdi
	neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push 0 ; stack-mem for string
	push 0
	push 0
	
	mov rcx, 10 ; base of system
	mov rax, rdi
	mov rdi, rsp
	add rdi, 3*8-1 ; offset to end stack-mem, mem fill from end
	.div_loop: 
		xor rdx, rdx 
		div rcx
		add dl, '0' ; % + '0' 
		dec rdi ; offset--
		mov [rdi], dl ; save %
		test rax, rax ; div is 0, all number is parsed
		jnz .div_loop
	
	call print_string
	
	add rsp, 3*8
	ret

		

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	mov rax, 1
	xor rcx, rcx
	.counter:
		inc rcx
		mov r8b, byte[rdi+rcx-1]
		cmp r8b, byte[rsi+rcx-1] ; cmp s1[i], s1[j]
		je .strings_equals
		xor rax, rax ; if (!=) => exit
		.strings_equals:
			test r8b, r8b ; if (s1[i] == 0) => exit
			jnz .string_a_not_end
				ret
		.string_a_not_end:
			cmp byte[rsi+rcx-1], 0 ; if (s2[i] == 0) => exit
			jne .counter
			ret
	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp ; pointer
    mov rdx, 1 ; length
    mov rax, READ_SYSCALL_NUMBER
    mov rdi, STD_IN
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;
; var buffer = [];
; var buffer_size = 17;
; buffer_size--;
; var b = 0
; while (1) {
;    var t = read();
;    if (t === undefined) break;
;    if (t == ' '||t == '\t'||t == '\n') {
;        if (b!=0)
;            break;
;    } else {
;        if (b >= buffer_size) { b = 0; break; }
;        buffer[b] = t;
;        b++;
;        buffer[b] = '\0';
;    }
; }
; console.log(buffer, b);

read_word:
	push rbx
	dec rsi
	push rsi
	push rdi
	xor rbx, rbx
	cmp rsi, 0
	jle .break
	mov byte[rdi], 0
	.reading_char:
		call read_char
		test rax, rax
		jz .break
		cmp rax, 9
		je .is_space_char
		cmp rax, 10
		je .is_space_char
		cmp rax, 32
		je .is_space_char
	.not_space_char:
		cmp rbx, [rsp+8]
		jge .not_successful_return
		pop rdi
		mov byte[rdi+rbx], al
		inc rbx
		mov byte[rdi+rbx], 0
		push rdi
		jmp .reading_char
	.is_space_char:
		test rbx, rbx
		jnz .break
		jmp .reading_char
	.break:
		mov rdx, rbx
		mov rax, [rsp]
		pop rdi
		pop rsi
		pop rbx
		ret
	.not_successful_return:
		mov qword[rsp], 0
		jmp .break
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
;
;
; s = mem[i]
; if ('0' < s < '9')
;   num *= 10
;   num += s - 0x30
; elif (i == 0)
;   ret 1
; else
;   break

parse_uint:
	mov r8, 10
	xor r9, r9
	xor rax, rax
	mov rcx, -1
	xor rdx, rdx
	
	.reading_char:
		inc rcx
		mov r9b, [rdi+rcx]
		cmp r9b, 0
		je .return
		cmp r9b, '0'
		jl .return
		cmp r9b, '9'
		jg .return
		sub r9b, '0'
		mul r8
		add rax, r9
		jmp .reading_char
	
	.return:
		mov rdx, rcx
		ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '-'
	je .neg
	jmp parse_uint
	.neg:
		inc rdi
		call parse_uint
		neg rax
		inc rdx
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	mov rax, -1
	.counter:
		inc rax
		cmp rax, rdx
		jl .is_fits
		xor rax, rax ; offset >= buffer.length
		ret
		.is_fits:
			mov r8b, byte[rdi+rax]
			mov byte[rsi+rax], r8b
			test r8b, r8b
			jnz .counter
	ret
